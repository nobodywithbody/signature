package signature

import java.util.*

enum class TraverseStatus {
    WAIT_DESCRIPTION,
    WAIT_LETTER,
}

class Traverse(private val s: Scanner) {
    private var state = TraverseStatus.WAIT_DESCRIPTION
    private val fontSize = s.nextInt()
    private val countLetters = s.nextInt()

    fun parse(): Font {
        val font = Font(fontSize)
        val letters = Array(countLetters) { Letter(fontSize) }
        var i = 0
        var widthLetter = 0

        while (s.hasNextLine()) {
            when (state) {
                TraverseStatus.WAIT_DESCRIPTION -> {
                    letters[i].sign = s.next().first()
                    widthLetter = s.nextInt()
                    s.skip("\n")
                    state = TraverseStatus.WAIT_LETTER
                }
                TraverseStatus.WAIT_LETTER -> {
                    for (j in 0 until fontSize) {
                        letters[i].appendLine(s.nextLine().substring(0, widthLetter))
                    }

                    state = TraverseStatus.WAIT_DESCRIPTION
                    i++
                }
            }
        }
        font.letters = letters

        return font
    }
}
package signature

class Letter(fontSize: Int) {
    var sign = ' '
    val width
        get() = letter.first().length

    var letter = Array(fontSize) { "" }
    var currentLine = 0

    fun appendLine(string: String) {
        letter[currentLine] = string
        currentLine++
    }
}
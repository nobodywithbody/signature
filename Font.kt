package signature

class Font(val fontSize: Int) {
    var letters = Array(1) { Letter(1) }

    val space: Letter
        get() {
            var aSpaceWidth = 0
            for (i in letters) {
                if (i.sign == 'a') {
                    aSpaceWidth = i.width
                }
            }

            val space = Letter(fontSize)
            space.letter = Array(fontSize) { " ".repeat(aSpaceWidth) }

            return space
        }

    fun findLetter(letter: Char): Letter {
        for (i in letters) {
            if (i.sign == letter) {
                return i
            }
        }

        return space
    }
}
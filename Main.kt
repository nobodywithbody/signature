package signature

import java.io.File
import java.util.*

fun main() {
    val romanScanner = Scanner(File("/Users/vladislav_arsenev/fonts/roman.txt"))
    val mediumScanner = Scanner(File("/Users/vladislav_arsenev/fonts/medium.txt"))

    val traverseRoman = Traverse(romanScanner)
    val traverseMedium = Traverse(mediumScanner)
    val roman = traverseRoman.parse()
    val medium = traverseMedium.parse()

    println("Enter name and surname:")
    val input = Scanner(System.`in`)
    val name = input.nextLine()

    println("Enter person's status:")
    val status = input.nextLine()

    val text = Text(2)
    text.writeLine(Line(roman, name))
    text.writeLine(Line(medium, status))

    for (i in 0 until text.height) {
        println(text.getRow(i))
    }
}
package signature

class Line(val font: Font, val content: String, val height: Int = font.fontSize, var width: Int = 0) {
    val contentWidth: Int
        get() {
            var sumOfWidth = 0
            for (i in content) {
                sumOfWidth += font.findLetter(i).width
            }

            return sumOfWidth
        }

    val lineWidth: Int
        get() = if (contentWidth > width) contentWidth else width

    fun applyXOffset(content: String): String {
        val offset = (lineWidth - contentWidth) / 2
        val rightOffset = when {
            lineWidth % 2 == 1 && contentWidth % 2 == 0 -> offset + 1
            contentWidth % 2 == 1 && lineWidth % 2 == 0 -> offset + 1
            else -> offset
        }

        return "${" ".repeat(offset)}$content${" ".repeat(rightOffset)}"
    }

    fun getRow(number: Int): String {
        var row = ""
        if (number >= font.fontSize && number <= height) {
            return applyXOffset(" ".repeat(contentWidth))
        }

        for (i in content) {
            row += font.findLetter(i).letter[number]
        }

        return applyXOffset(row)
    }
}

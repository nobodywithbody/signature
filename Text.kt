package signature

class Text(val countLines: Int = 0, val borderSign: String = "8") {
    val BORDER_WIDTH = 1

    val lines = Array(countLines) { Line(Font(1), "") }
    val widthLine: Int
        get() {
            var maxWidth = 0
            for (line in lines) {
                if (maxWidth < line.lineWidth) {
                    maxWidth = line.lineWidth
                }
            }

            return maxWidth
        }
    var currentLine = 0

    val height: Int
        get() {
            var sumFontSizes = 0

            for (line in lines) {
                sumFontSizes += line.height
            }

            return sumFontSizes + BORDER_WIDTH * 2
        }

    fun normalizeLines() {
        for (line in lines) line.width = widthLine
    }

    fun writeLine(line: Line) {
        lines[currentLine] = line
        currentLine++
        normalizeLines()
    }

    private fun getRowOfLine(numberOfRow: Int): String {
        var linesHeight = 0
        for (i in 0 until countLines) {
            val height = lines[i].height
            linesHeight += height

            if (numberOfRow <= linesHeight) {
                return lines[i].getRow(numberOfRow - (linesHeight - height) - 1)
            }
        }

        return ""
    }

    fun getRow(number: Int): String = when (number) {
        0, height - 1 -> borderSign.repeat(widthLine + 8)
        else -> "${borderSign.repeat(2)}  ${getRowOfLine(number)}  ${borderSign.repeat(2)}"
    }
}